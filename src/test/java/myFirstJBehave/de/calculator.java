package myFirstJBehave.de;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class calculator {

    private char sign;
    private long number1;
    private long number2;
    private long result;

    @Given("a Calculator Class with the operations sign as <sign>")
    public void givenACalculatorClassWithTheOperationsSignAssign(@Named("sign") String sign) {
        this.sign = sign.charAt(0);

        switch (this.sign){
            case '+':
                System.out.println("addition operation");
                break;
            case '-':
                System.out.println("subtraction operation");
                break;
            case '*':
                System.out.println("multiplication operation");
                break;
            case '/':
                System.out.println("division operation");
                break;
            default:
                System.out.println("please give a correct operation");
        }
    }

    @When("I give the frist number as <number1>")
    public void whenIGiveTheFristNumberAsnumber1(@Named("number1") long number1) {
        this.number1 = number1;
    }

    @When("I give the second number as <number2>")
    public void whenIGiveTheSecondNumberAsnumber2(@Named("number2") long number2) {
        this.number2 = number2;
    }

    @Then("I should get the result as <result>")
    public void thenIShouldGetTheResultAsresult(@Named("result") long result) {
        Operations operations = new Operations();
        switch (this.sign){
            case '+':
                this.result = operations.addition(this.number1,this.number2);
                Assert.assertEquals(this.result,result);
                break;
            case '-':
                this.result = operations.subtraction(this.number1,this.number2);
                Assert.assertEquals(this.result,result);
                break;
            case '*':
                this.result = operations.multiplication(this.number1,this.number2);
                Assert.assertEquals(this.result,result);
                break;
            case '/':
                this.result = operations.division(this.number1,this.number2);
                Assert.assertEquals(this.result,result);
                break;
             default:
                 System.out.println("please give a correct operation");
        }
    }


}
