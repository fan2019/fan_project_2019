
Narrative:
As a user
I want to make an Operations
So that I can get a results of two numbers

Scenario: scenario addition
Given a Calculator Class with the operations sign as <sign>
When I give the frist number as <number1>
And I give the second number as <number2>
Then I should get the result as <result>

Examples:
|sign|number1|number2|result|
|+|5|3|8|

Scenario: scenario subtraction
Given a Calculator Class with the operations sign as <sign>
When I give the frist number as <number1>
And I give the second number as <number2>
Then I should get the result as <result>

Examples:
|sign|number1|number2|result|
|-|5|3|1|

Scenario: scenario multiplication
Given a Calculator Class with the operations sign as <sign>
When I give the frist number as <number1>
And I give the second number as <number2>
Then I should get the result as <result>

Examples:
|sign|number1|number2|result|
|*|5|3|15|

Scenario: scenario division
Given a Calculator Class with the operations sign as <sign>
When I give the frist number as <number1>
And I give the second number as <number2>
Then I should get the result as <result>

Examples:
|sign|number1|number2|result|
|/|6|3|2|